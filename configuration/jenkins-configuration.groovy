jenkins {
    // General environment variables
    envVars {

    }

    auth {
        disable = true
    }
    // Google authentication parameters
//    auth {
//        domain = ""
//        clientSecret = ""
//        clientId = ""
//    }
    // Slack integration
//    slack {
//        domain = ""
//        jenkinsUrl = ""
//        token = ""
//    }
    // SSH private keys
    ssh {
        bitbucket {
            content = """\
                -----BEGIN RSA PRIVATE KEY-----
                MIIEpQIBAAKCAQEAmBLCwszlaVG5rH/zX1/XuNZkMzUKr/f3Do6GEbsOrRjRQvOb
                8lMHTAjR8DP+v4tDY9FaUYIWhKPZ8I/GbHa+cq4BZfFJH4LxWI74I+vHgUAtWnAT
                XV+njfDMC3MGI26vvpXJz0t3q8AaGAw2JsG+r3Z9gxKwPaB+O62ybtIWntCVf7cc
                GVfcO1jGB6Us1GfkhamH3mIAcw7bozci8SRpNw84kcYMZvOray7QN07V6HKjq8MG
                iWg9SDTzNFl3Z1pRb/lDGISKH5O9yUjhu0MHY6nrOhOfQFCLQ8UOhjOJ210BYYZF
                ZEIuTdpYWDbnR3Brr0jOMnAzeBXH5OfJ4OiaWQIDAQABAoIBAQCDwz2dK4taxcTr
                AbINYS24qYoIFF0+OsuFgVgI5NrCgE5QhGMKg4Y/CyVbyrFfpXquciafmOeJ5a5g
                ETMLp5c8Jihv+7Av7XTQFpBKD3C9PK4PPiyJKugyxAEHqpIXAUClEYPaaLk3xyrU
                yz/gNN7TiQcXGoo522WEr+t6ue1/jEUO9R7+jozp4WYcoYHLcCCDoH84iHFf2Uso
                OyNyLa/WKDkmhGfP7MYjpRzQhUUG8iht5UE2zKLaPvmY3LfR7FvS0csBj5DeOC7E
                3hCzczMlHkQWeGoPQOXVUc8yuYztnrCzzKHi+rSuvdm0jOvm3byfEQ1360+SbwSQ
                dkoyMH5BAoGBAMbE8SvRoQcw6SJUXaxR0eFWkTCZaTnUAHiC1qM6d4u+j5vxbLor
                mtpz/3afwMhLW/Yv1wKE24FGKFHVjcxeazQl1UCGq7hKlDbKHGceIGYSv+lJjNC+
                sy/UlbSC270bSG/8/uZc8KjahQmCW94QW9F1GDrmmW0i675yCQj87t0XAoGBAMPb
                6JJYrS/RdWj+MO50Xfu8wn1jFr0P9+d70C4krEdqr0t7f/32Aab7dlOm1nMfLyIx
                7IZOyMcV4K8cw/JhbCGMlf1u4w01HFJs1IYTfOkx9nHw1LU5pg/0hkBiqdELxIIE
                YzT6D7OSsx1p17dBR6gp03eg/QsoYD/W3qIfp0oPAoGBAIyjGAjq/8Z4wtt5WfXs
                xlVrzLrYMO2rpNGfsYI4ghEsOawau9VVuPFMFbq2+c4+E8kyvCyaGD11iF2ufe/V
                9oAlLKfFIYb5rbi+dC450K6oIYVMdwfThjEXdv0LgbJhtXUXSrtvkw/F3U+HHr0W
                UgD3mrdCJdhkVuye/umPHOrtAoGBAI3iHNUHkE53ukpY+jGD8yGOWIZk5fAIRVpY
                AzOqNXN3AY345XugXwoMWGe5ENlM2eQ3rlSNdj1ix+WTPQ+1K5QSOAXj8m967g13
                8W0/aNm17AYWpMC0BmzAgxDZaJObprtZWlTiYcWAnGjOaArFK5fS1PJwmV8f3Lni
                el2P+KxZAoGAVvob2HYDks09fI18x3Ar3WKd2J/i/x6DZ+AnferGuBd7x+M7Qg2g
                0iycdOP+E4VO4Xi9c6oOuhlHoSotubSUqIGnGr/R/N9m0J9SE/QmpPwKATTlri0a
                OiS5DTp/RlyLyx9KgIO2YumOkoiJLo1dLUXikcZwiuY8Ln11k4OCcl8=
                -----END RSA PRIVATE KEY-----
            """.stripIndent()
            passphrase = "QxG4xvdk"
        }
    }
//    // kubernetes configuration
//    kubernetes {
//        // Configuration name
//        name = "kubernetes"
//        // Kubernetes api url. Using local as slaves are expected to run in the same machine
//        serverUrl = "https://kubernetes.default.svc.cluster.local"
//        // Namespace that build slaves will belong to
//        namespace = "build-server"
//        // Jenkins master URL. Since master runs in the same cluster and also belongs to
//        // build-server namespace, slaves can access it by using only it's service name.
//        jenkinsUrl = 'http://jenkins-master'
//        // The maximum number of concurrently running slave containers that Kubernetes is allowed to run
//        containerCap = "5"
//        connectTimeout = 5
//        readTimeout = 15
//        retentionTimeout = 0
//        defaultTemplate = "klarity-slave"
//    }

    seed {
        repository = "git@bitbucket.org:german_labs/jobs.git"
        credentials = "bitbucket"
    }
}
